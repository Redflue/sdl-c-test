
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"
#include "SDL2/SDL_image.h"

#define SW 700 //Screen Width
#define SH 800 //Screen Height
#define GRAVITY 0.004

int initSDL() {

    if (SDL_Init(SDL_INIT_EVENTS | SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0) {
        printf("SDL INIT Error! :: %s", SDL_GetError());
        return -1;
    }
    if (TTF_Init() < 0) {
        printf("TTF Init Error! :: %s", TTF_GetError());
        return -1;
    }
    if (IMG_Init(IMG_INIT_PNG) < 0) {
        printf("IMG Init Error! :: %s", IMG_GetError());
        return -1;
    }

    return 0;
}

typedef struct {
    float x;
    float y;
    float lastx;
    float lasty;
    float rad;
    Uint8 r;
    Uint8 g;
    Uint8 b;
} ball;

SDL_Surface* pCircleImg;
SDL_Texture* pCircleTex;
void drawCircle(SDL_Renderer *renderer, int posx, int posy, int radius, Uint8 r, Uint8 g, Uint8 b) {
    if (!pCircleTex) {
        printf("no image");
        return;
    }

    SDL_SetTextureColorMod(pCircleTex, r, g, b);
    SDL_RenderCopyEx(renderer, pCircleTex, NULL, &(SDL_Rect){posx - radius, posy - radius, radius * 2, radius * 2}, 0, NULL, SDL_FLIP_NONE);

    //SDL_SetSurfaceColorMod(pCircleImg, r, g, b);
    //SDL_UpperBlitScaled(pCircleImg, NULL, dst, &(SDL_Rect){posx - radius, posy - radius, radius*2, radius*2});
}


void renderTextSurface(SDL_Renderer* renderer, SDL_Surface *textSurface, const SDL_Rect* dstRect) {
    SDL_Texture* textTex;
    textTex = SDL_CreateTextureFromSurface(renderer, textSurface);

    if (textTex) {
        SDL_RenderCopyEx(renderer, textTex, NULL, dstRect, 0, NULL, SDL_FLIP_NONE);
    }

    SDL_DestroyTexture(textTex);
}

void renderTextTexture(SDL_Renderer* renderer, SDL_Texture* textTexture, const SDL_Rect* dstRect) {
    if (textTexture)
        SDL_RenderCopyEx(renderer, textTexture, NULL, dstRect, 0, NULL, SDL_FLIP_NONE);
}

void drawBall(SDL_Renderer *renderer, ball* b) {
    drawCircle(renderer, b->x, b->y, b->rad, b->r, b->g, b->b);
}

void drawRect(SDL_Surface* dst, int posx, int posy, int w, int h, Uint8 r, Uint8 g, Uint8 b) {

}

#define circleResolution 24
void drawCircleOutline(SDL_Renderer* renderer, int posx, int posy, int rad, Uint8 r, Uint8 g, Uint8 b, int lineSize) {
    float angle = 0;
    SDL_SetRenderDrawColor(renderer, r, g, b, 0xff);
    rad -= (lineSize - 1);
    for (char j = 0; j < lineSize; j++) {
        for (char i = 0; i < circleResolution; i++) {
            SDL_RenderDrawLineF(renderer, posx + cosf(angle) * rad, posy + sinf(angle) * rad, posx + cosf(angle + M_PI * 2 / circleResolution) * rad, posy + sinf(angle + M_PI * 2 / circleResolution) * rad);
            angle += M_PI * 2 / circleResolution;
        }
        rad++;
    }
}


int main(int argc, char** argv) {
    if (!initSDL()) {
        SDL_Window* window = NULL;
        SDL_Renderer *renderer = NULL;

        TTF_Font* font;


        if (SDL_CreateWindowAndRenderer(SW, SH, SDL_WINDOW_SHOWN, &window, &renderer) < 0) {
            printf("damn, all these problems... %s\n", SDL_GetError());
            SDL_DestroyWindow(window);
            SDL_Quit();
            return -1;
        } else {
            font = TTF_OpenFont("fonts/Roboto-Regular.ttf", 22);

            pCircleImg = IMG_Load("imgs/circle.png");
            pCircleTex = SDL_CreateTextureFromSurface(renderer, pCircleImg);

            SDL_Event e;
            bool quit = false;

            int maxBalls = 128;
            int currentBalls = 0;

            ball* balls = malloc(sizeof(ball) * maxBalls);
            if (!balls) {
                SDL_DestroyWindow(window);
                SDL_Quit();
                return -1;
            }

            float delta = 1;
            float lastDelta = 1;

            Uint64 NOW = SDL_GetPerformanceCounter();
            Uint64 LAST = 0;

            /*TEST*/
            currentBalls = 1;
            {
                ball* b = &(balls[0]);
                if (b) {
                    b->x = SW / 2;
                    b->y = 0;
                    b->lastx = b->x;
                    b->lasty = b->y;
                    b->rad = 50;
                    b->r = 255;
                    b->g = 120;
                    b->b = 50;
                }
            }
            /*END*/

            char* text = malloc(sizeof(char) * 255);
            SDL_Texture* textTexture = NULL;
            {
                if (text)
                    sprintf_s(text, 255, "%d/%d balls", currentBalls, maxBalls);
                SDL_Surface* textSurface = TTF_RenderText_Blended(font, text, (SDL_Color) { 20, 20, 20, 150 });
                textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
                SDL_FreeSurface(textSurface); 
            }

            int mousex = 0, mousey = 0;

            while (quit == false) {

                SDL_SetRenderDrawColor(renderer, 230, 230, 245, 255);
                SDL_RenderFillRect(renderer, NULL);

                SDL_GetMouseState(&mousex, &mousey);

                while (SDL_PollEvent(&e)) {
                    if (e.type == SDL_QUIT) {
                        quit = true;
                    }
                    if (e.type == SDL_MOUSEBUTTONDOWN) {

                        ball* b = &(balls[currentBalls]);

                        if (b && currentBalls < maxBalls) {
                            b->x = mousex;
                            b->y = mousey;
                            b->lastx = b->x;
                            b->lasty = b->y;
                            b->rad = 50;
                            b->r = 50;
                            b->g = 255;
                            b->b = 150;

                            currentBalls++;
                        }

                        if (textTexture != NULL) {
                            SDL_DestroyTexture(textTexture);
                            textTexture = NULL;
                        }

                        if (font) {
                            if (text)
                                sprintf_s(text, 255, "%d/%d balls", currentBalls, maxBalls);
                            SDL_Surface* textSurface = TTF_RenderText_Blended(font, text, (SDL_Color) { 20, 20, 20, 150 });
                            textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
                            SDL_FreeSurface(textSurface);
                        }
                    }
                }

                if (textTexture) {
                    if (text)
                        sprintf_s(text, 255, "%d/%d balls", currentBalls, maxBalls);
                    //SDL_Surface* textSurface = TTF_RenderText_Blended(font, text, (SDL_Color) { 20, 20, 20, 150 });
                    int w, h;
                    SDL_QueryTexture(textTexture, NULL, NULL, &w, &h);
                    renderTextTexture(renderer, textTexture, &(SDL_Rect){ 0, 0, w, h });
                    
                    //SDL_FreeSurface(textSurface);
                }

                if (currentBalls > 0) {
                    for (int i = 0; i < currentBalls; i++) {
                        ball* b = &(balls[i]);
                        if (b) {
                            float lastx = b->x;
                            float lasty = b->y;

                            b->y = b->y + (b->y - b->lasty) * (delta / lastDelta) + GRAVITY * delta * delta;
                            b->x = b->x + (b->x - b->lastx) * (delta / lastDelta);

                            for (int o = 0; o < currentBalls; o++) {
                                ball* ob = &(balls[o]);
                                if (o != i) {
                                    float dst = 1 + sqrtf(powf(ob->x - b->x, 2) + powf(ob->y - b->y, 2));
                                    float radii = b->rad + ob->rad;
                                    if (dst < radii) {
                                        //ob->r = 255;
                                        float distanceToResolve = radii - dst;

                                        float deltaX = ob->x - b->x;
                                        float deltaY = ob->y - b->y;

                                        float normalX = deltaX / max(dst, 0.1);
                                        float normalY = deltaY / max(dst, 0.1);

                                        b->x -= normalX * distanceToResolve / 2;
                                        b->y -= normalY * distanceToResolve / 2;

                                        ob->x += normalX * distanceToResolve / 2;
                                        ob->y += normalY * distanceToResolve / 2;
                                    }
                                }
                            }

                            if (b->y > SH - b->rad)
                                b->y = SH - b->rad;
                            if (b->x < b->rad)
                                b->x = b->rad;
                            if (b->x > SW - b->rad)
                                b->x = SW - b->rad;
                            if (b->y < 0 + b->rad)
                                b->y = 0 + b->rad;

                            drawBall(renderer, b);
                            drawCircleOutline(renderer, b->x, b->y, b->rad + 0.5, 0, 0, 0, 1);

                            b->lastx = lastx;
                            b->lasty = lasty;
                        }
                    }
                }

                drawCircleOutline(renderer, mousex, mousey, 50, 0xff, 0x00, 0x00, 1);

                SDL_RenderPresent(renderer);

                LAST = NOW;
                NOW = SDL_GetPerformanceCounter();
                lastDelta = delta;
                delta = (double)((NOW - LAST) * 1000 / (double)SDL_GetPerformanceFrequency());
            }
        }
        SDL_DestroyWindow(window);
        SDL_Quit();
    }
    else {
        return -1;
    }

	return 0;
}